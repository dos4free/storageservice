const util = require('util')
const igdb = require('igdb-api-node').default;

const client = igdb(process.env.IGDB_KEY_ID, process.env.IGDB_ACCESS_KEY);

function searchIgdb(gameName){
    return client
    .fields([
        'name',
        'slug',
        'storyline',
        'total_rating',
        'genres.id, genres.name, genres.slug',
        'involved_companies.game, involved_companies.developer, involved_companies.publisher',
        'involved_companies.company.id, involved_companies.company.name, involved_companies.company.slug',
        'screenshots.*'
    ]) 
    .limit(10) // limit to 50 results
    .search(gameName) // search for a specific name (search implementations can vary)
    .request('/games');
}

function extractKey(record){
    return record.s3.object.key;
}

function extractFileName(key){
    let splitKeys = key.split("/");
    return splitKeys.pop();
}

function getSearchTerm(fileName){
    return fileName.replace(".zip","").split(".").join(" ");
}

function getSearchTermFromRecord(record){

    let fileKey = extractKey(record);
    console.log(`Got fileKey ${fileKey}`);
    let fileName = extractFileName(fileKey);
    console.log(`Got fileName ${fileName}`);
    let searchTerm = getSearchTerm(fileName);
    console.log(`Got searchTerm "${searchTerm}"`);
    return searchTerm;
}

exports.handler = async (event, context, callback) => {
    const promises = [];
    for (let idx = 0;idx < event.Records.length;idx++){
        let record = event.Records[idx];
        searchTerm = getSearchTermFromRecord(record);
        promises.push(searchIgdb(searchTerm));
    }
    
    const outputs = await Promise.all(promises);
    outputs.forEach((res) => {
        if(res.data.length == 0){
            console.log("Got zero results"); 
        }
        else if(res.data.length == 1){
            console.log("Got exactly one game");
            console.log(JSON.stringify(res.data[0], null, 4));    
        }else
        {
            console.log("Multiple games found");
            res.data.forEach((value, key) => {
                console.log(JSON.stringify(value, null, 4));
            });
        }        

    });
    console.log("Done!");
}
